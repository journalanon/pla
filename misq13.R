source("plautils.R")

library(plyr)
library(ggplot2)
library(scales)
library(Rgraphviz)
library(clue) # Contains an implementation of the Hungarian method for the linear sum assignment problem
library(RecordLinkage) # Contains an implementation of the Levenshtein distance
library(xtable)

###
### ENTER YOUR CREDENTIALS HERE
###
plaremote("YOURUSER", "YOURPASS")

### NOTHING TO CONFIGURE BELOW THIS LINE ###

##########################################################################################
#
# PART 1: Metrics for market quality (competition vs monopoly comparisons)
#
##########################################################################################

PURCHASE <- c(0,1);
PARTICIPANTS <- c(0,3,5,8); # 0 stands for unconstrained

##########################################################################################
#
# Analysis 1) Tariff Transactions
#
# For each pair of competitive / monopoly game from the 2013 finals, compare
# the HHI index, the retail price index, the retail price variability, 
# and the total retail volume.
#
##########################################################################################

mq_tfc_analysis <- plaf("
  SELECT    gameid, 
			      participants,
            comp.purchase purchase, 
            comp.hhi comp_hhi,
            mono.hhi mono_hhi,
            comp.retail_price_level comp_retail_price_level,
            mono.retail_price_level mono_retail_price_level,
            comp.retail_price_variability comp_retail_price_variability,
            mono.retail_price_variability mono_retail_price_variability,
            comp.total_retail_kwh comp_total_retail_kwh,
            mono.total_retail_kwh mono_total_retail_kwh
  FROM
            tmp.cmpmap m,
			      tmp.validcmp c,
            tmp.analysis_tfc comp,
            tmp.analysis_tfc mono
  WHERE
			      m.competitive = c.competition AND
            m.competitive = comp.competition AND
            m.monopoly = mono.competition AND
            comp.purchase = mono.purchase;")

mq_hhi <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
mq_rprice <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
mq_rpricevar <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
mq_totalretail <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));

# Note: None of these datasets follow a normal distribution, in particular because prices
# and market shares are about constant under the monopolistic setting. It is therefor not
# okay to interpret the p-values of the paire t-tests below.

for(p in 1:length(PURCHASE)){
  for(s in 1:length(PARTICIPANTS)){
    
    if(PARTICIPANTS[s] == 0){
      rowIndices = mq_tfc_analysis$purchase == PURCHASE[p];
    } else {
      rowIndices = mq_tfc_analysis$purchase == PURCHASE[p] &
                   mq_tfc_analysis$participants == PARTICIPANTS[s];
    }
    
    mq_hhi[[p,s]] <- t.test(
      mq_tfc_analysis[rowIndices,]$comp_hhi, 
      mq_tfc_analysis[rowIndices,]$mono_hhi, 
      paired=TRUE); 
    
    mq_rprice[[p,s]] <- t.test(
      mq_tfc_analysis[rowIndices,]$comp_retail_price_level, 
      mq_tfc_analysis[rowIndices,]$mono_retail_price_level, 
      paired=TRUE); 
    
    mq_rpricevar[[p,s]] <- t.test(
      mq_tfc_analysis[rowIndices,]$comp_retail_price_variability, 
      mq_tfc_analysis[rowIndices,]$mono_retail_price_variability, 
      paired=TRUE); 
    
    mq_totalretail[[p,s]] <- t.test(mq_tfc_analysis[rowIndices,]$comp_total_retail_kwh / 
                                    mq_tfc_analysis[rowIndices,]$mono_total_retail_kwh);
  }
}

##########################################################################################
#
# Analysis 2) Distribution Transactions
#
# Compare the development of the load factor between the monopolistic and the
# competitive scenario
#
##########################################################################################

mq_dtr_analysis <- plaf("
  SELECT
    vcmp.participants,
    comp.dtr_o_avg_kwh / comp.dtr_o_max_kwh comp_load_factor, 
    mono.dtr_o_avg_kwh / mono.dtr_o_max_kwh mono_load_factor
  FROM
    tmp.cmpmap,
    tmp.validcmp vcmp,
    tmp.dtr_overall_stats comp,
    tmp.dtr_overall_stats mono
  WHERE
    tmp.cmpmap.competitive = comp.competition AND
    tmp.cmpmap.competitive = vcmp.competition AND
    tmp.cmpmap.monopoly = mono.competition AND
    comp.dtr_o_purchase = 1 AND
    mono.dtr_o_purchase = 1;")

mq_lfactor <- array(list(), c(length(PARTICIPANTS)));

# Note: None of these datasets follow a normal distribution, in particular because prices
# and market shares are about constant under the monopolistic setting. It is therefor not
# okay to interpret the p-values of the paire t-tests below.

for(s in 1:length(PARTICIPANTS)){
  
  if(PARTICIPANTS[[s]] == 0){
    rowIndices = T;
  } else {
    rowIndices = mq_dtr_analysis$participants == PARTICIPANTS[s];
  }
  
  mq_lfactor[[s]] <- t.test(
    mq_dtr_analysis[rowIndices,]$comp_load_factor, 
    mq_dtr_analysis[rowIndices,]$mono_load_factor, 
    paired=TRUE); 
}

##########################################################################################
#
# Analysis 3) Balancing Quality
#
# Compare the ratio of needed balancing to overall wholesale market transactions, and
# the price for balancing versus the price for wholesale energy
#
##########################################################################################

mq_bal_quality_analysis <- plaf("
  SELECT
      vcmp.participants participants,
    	comp.purchase purchase,
      comp.balancing_ratio comp_bal_ratio,
    	mono.balancing_ratio mono_bal_ratio,
      comp.balancing_price_ratio comp_bal_price_ratio,
    	mono.balancing_price_ratio mono_bal_price_ratio
  FROM
      tmp.cmpmap,
      tmp.validcmp vcmp,
      tmp.analysis_bal_quality comp,
      tmp.analysis_bal_quality mono
  WHERE
      tmp.cmpmap.competitive = comp.competition AND
      tmp.cmpmap.competitive = vcmp.competition AND
      tmp.cmpmap.monopoly = mono.competition AND
  	comp.purchase = mono.purchase;")

mq_balratio <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
mq_balprratio <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));

for(p in 1:length(PURCHASE)){
  for(s in 1:length(PARTICIPANTS)){
    
    if(PARTICIPANTS[s] == 0){
      rowIndices = mq_bal_quality_analysis$purchase == PURCHASE[p];
    } else {
      rowIndices = mq_bal_quality_analysis$purchase == PURCHASE[p] &
        mq_bal_quality_analysis$participants == PARTICIPANTS[s];
    }
    
    if(PARTICIPANTS[[s]] == 0){
      rowIndices = T;
    } else {
      rowIndices = mq_bal_quality_analysis$participants == PARTICIPANTS[s];
    }
    
    mq_balratio[[p,s]] <- t.test(
      mq_bal_quality_analysis[rowIndices,]$comp_bal_ratio, 
      mq_bal_quality_analysis[rowIndices,]$mono_bal_ratio, 
      paired=TRUE); 
    
    mq_balprratio[[p,s]] <- t.test(
      mq_bal_quality_analysis[rowIndices,]$comp_bal_price_ratio, 
      mq_bal_quality_analysis[rowIndices,]$mono_bal_price_ratio, 
      paired=TRUE); 
  }
}

##########################################################################################
#
# PART 2: Metric for process quality (2012 vs 2013 comparison)
#
##########################################################################################

# First, because the 2012 and 2013 competitions do not match, broker-wise, we look for
# an optimal assignment between the two sets of competitons:
# 
# The idea is as follows:
# 
# Let C_1 = { c_11, c_12, ..., c_1n} and 
#     C_2 = { c_21, c_22, ..., c_2m}
# 
# be two non-empty set of competitions with possibly different sizes (we assume 
# n <= m wlog), and
# 
# f(c_i,c_j) a similarity measure. Then the task is to find the mapping between 
# C_1 and C_2 that uniquely associates each c \in C_1 with a c' \in C_2 and that
# minimizes the sum over all mapped f(c,c'), while leaving m - n competitions from 
# C_2 unassigned.
#
# This can be written as a constrained integer program over an assignment matrix A
# where A_ij = 1 indicates that c_1i should be connected to c_2j. Obviously,
# all rows must sum to exactly one, and all columns must sum to one or zero.
# 

C1 <- plaf("SELECT * 
            FROM
           tmp.validcmp_signatures
           WHERE
           comment = '2012 Power TAC Nuremberg Demo Games'");

C2 <- plaf("SELECT * 
            FROM
              tmp.validcmp_signatures
            WHERE
              comment = '2013 Power TAC Finals'");

N1 <- nrow(C1); N2 <- nrow(C2); N <- N1 * N2;
D <- matrix(0, N1, N2);

dist.f <- function (c1, c2) {
  if(nchar(c1$signature) != nchar(c2$signature)){
    return(999);
  } else {
    return(levenshteinDist(c1$signature, c2$signature));
  }
}

for (i in 1:nrow(C1)){
  for (j in 1:nrow(C2)){
    D[i,j] <- dist.f(C1[i,], C2[j,]);
  }
}

# Compute the optimal assignment and write the result to a table
assignment <- solve_LSAP(D);

# Approximate number of simulated years in the matched games
yearsPlayed <- (sum(C1$length) + sum(C2[assignment,]$length)) / 24 / 30 / 12;

result <- list();
result$pilot2012 <- C1$competition;
result$finals2013 <- C2[assignment,]$competition;

# Uncomment this to persist the mapping to the database (works only if you have sufficient privileges)
# dbWriteTable(placon, "tmp.cmpmap1213", as.data.frame(result), row.names = F, overwrite = T);

rm(C1, C2, D, i, j, N1, N2, N, assignment, result);

PURCHASE <- c(0,1);
PARTICIPANTS <- c(0,3,5); # 0 stands for unconstrained

##########################################################################################
#
# Analysis 1) Tariff Transactions
#
##########################################################################################

pq_tfc_analysis <- plaf("
  SELECT    participants,
            finals.purchase purchase, 
            finals.hhi finals_hhi,
            pilot.hhi pilot_hhi,
            finals.retail_price_level finals_retail_price_level,
            pilot.retail_price_level pilot_retail_price_level,
            finals.retail_price_variability finals_retail_price_variability,
            pilot.retail_price_variability pilot_retail_price_variability,
            finals.total_retail_kwh finals_total_retail_kwh,
            pilot.total_retail_kwh pilot_total_retail_kwh
  FROM
            tmp.cmpmap1213 m,
  		      tmp.validcmp c,
            tmp.analysis_tfc finals,
            tmp.analysis_tfc pilot
  WHERE
            m.finals2013 = finals.competition AND
            m.pilot2012 = pilot.competition AND
            finals.purchase = pilot.purchase AND
            c.competition = finals.competition;")

pq_hhi <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
pq_rprice <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
pq_rpricevar <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
pq_totalretail <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));

for(p in 1:length(PURCHASE)){
  for(s in 1:length(PARTICIPANTS)){
    
    if(PARTICIPANTS[s] == 0){
      rowIndices = pq_tfc_analysis$purchase == PURCHASE[p];
    } else {
      rowIndices = pq_tfc_analysis$purchase == PURCHASE[p] &
                   pq_tfc_analysis$participants == PARTICIPANTS[s];
    }
    
    pq_hhi[[p,s]] <- t.test(
      pq_tfc_analysis[rowIndices,]$finals_hhi, 
      pq_tfc_analysis[rowIndices,]$pilot_hhi, 
      paired=TRUE); 
    
    pq_rprice[[p,s]] <- t.test(
      pq_tfc_analysis[rowIndices,]$finals_retail_price_level, 
      pq_tfc_analysis[rowIndices,]$pilot_retail_price_level, 
      paired=TRUE); 
    
    pq_rpricevar[[p,s]] <- t.test(
      pq_tfc_analysis[rowIndices,]$finals_retail_price_variability, 
      pq_tfc_analysis[rowIndices,]$pilot_retail_price_variability, 
      paired=TRUE); 
    
    pq_totalretail[[p,s]] <- t.test(pq_tfc_analysis[rowIndices,]$finals_total_retail_kwh / 
                                    pq_tfc_analysis[rowIndices,]$pilot_total_retail_kwh);
  }
}

##########################################################################################
#
# Analysis 2) Innovative Tariff Models
#
##########################################################################################

pq_tfc_types_analysis <- plaf("
    SELECT  participants,
            finals.purchase purchase, 
            finals.innovative_share finals_innovative_share,
            pilot.innovative_share pilot_innovative_share
    FROM
            tmp.cmpmap1213 m,
  		      tmp.validcmp c,
            tmp.analysis_tfc_types finals,
            tmp.analysis_tfc_types pilot
    WHERE
            m.finals2013 = finals.competition AND
            m.pilot2012 = pilot.competition AND
            finals.purchase = pilot.purchase AND
            c.competition = finals.competition;");

pq_innotrf <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));

for(p in 1:length(PURCHASE)){
  for(s in 1:length(PARTICIPANTS)){
    
    if(PARTICIPANTS[s] == 0){
      rowIndices = pq_tfc_types_analysis$purchase == PURCHASE[p];
    } else {
      rowIndices = pq_tfc_types_analysis$purchase == PURCHASE[p] &
        pq_tfc_types_analysis$participants == PARTICIPANTS[s];
    }
    

    pq_innotrf[[p,s]] <- t.test(
      pq_tfc_types_analysis[rowIndices,]$finals_innovative_share, 
      pq_tfc_types_analysis[rowIndices,]$pilot_innovative_share, 
      paired=TRUE); 
  }
}


##########################################################################################
#
# Analysis 3) Balancing Quality
#
##########################################################################################

pq_bal_quality_analysis <- plaf("
  SELECT  
    participants,
    finals.purchase purchase, 
    finals.balancing_ratio finals_balancing_ratio,
    pilot.balancing_ratio pilot_balancing_ratio,
  finals.balancing_price_ratio finals_balancing_price_ratio,
    pilot.balancing_price_ratio pilot_balancing_price_ratio
  FROM
    tmp.cmpmap1213 m,
    tmp.validcmp c,
    tmp.analysis_bal_quality finals,
    tmp.analysis_bal_quality pilot
  WHERE
    m.finals2013 = finals.competition AND
    m.pilot2012 = pilot.competition AND
    finals.purchase = pilot.purchase AND
    c.competition = finals.competition;");

pq_balratio <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));
pq_balprratio <- array(list(), c(length(PURCHASE), length(PARTICIPANTS)));

for(p in 1:length(PURCHASE)){
  for(s in 1:length(PARTICIPANTS)){
    
    if(PARTICIPANTS[s] == 0){
      rowIndices = pq_bal_quality_analysis$purchase == PURCHASE[p];
    } else {
      rowIndices = pq_bal_quality_analysis$purchase == PURCHASE[p] &
        pq_bal_quality_analysis$participants == PARTICIPANTS[s];
    }
    
    pq_balratio[[p,s]] <- t.test(
      pq_bal_quality_analysis[rowIndices,]$finals_balancing_ratio, 
      pq_bal_quality_analysis[rowIndices,]$pilot_balancing_ratio, 
      paired=TRUE); 
    
    pq_balprratio[[p,s]] <- t.test(
      pq_bal_quality_analysis[rowIndices,]$finals_balancing_price_ratio, 
      pq_bal_quality_analysis[rowIndices,]$pilot_balancing_price_ratio, 
      paired=TRUE); 
  }
}

##########################################################################################
#
# Analysis 4) Sourcing/Margin Analysis
#
##########################################################################################

pq_sourcing_margin_analysis <- plaf("
  SELECT  
    participants,
    finals.sourced_wholesale finals_sourced_wholesale,
    pilot.sourced_wholesale pilot_sourced_wholesale,
    finals.sourced_retail finals_sourced_retail,
    pilot.sourced_retail pilot_sourced_retail,
    finals.margin_wholesale finals_margin_wholesale,
    pilot.margin_wholesale pilot_margin_wholesale,
	  finals.margin_retail finals_margin_retail,
    pilot.margin_retail pilot_margin_retail
  FROM
    tmp.cmpmap1213 m,
    tmp.validcmp c,
    tmp.analysis_margin_sourcing finals,
    tmp.analysis_margin_sourcing pilot
  WHERE
    m.finals2013 = finals.competition AND
    m.pilot2012 = pilot.competition AND
    c.competition = finals.competition;");

pq_wsource <- array(list(), length(PARTICIPANTS));
pq_rsource <- array(list(),length(PARTICIPANTS));
pq_wmarg <- array(list(), length(PARTICIPANTS));
pq_rmarg <- array(list(),length(PARTICIPANTS));

for(s in 1:length(PARTICIPANTS)){
  
  if(PARTICIPANTS[s] == 0){
    rowIndices = T;
  } else {
    rowIndices = pq_sourcing_margin_analysis$participants == PARTICIPANTS[s];
  }
  
  pq_wsource[[s]] <- t.test(
    pq_sourcing_margin_analysis[rowIndices,]$finals_sourced_wholesale, 
    pq_sourcing_margin_analysis[rowIndices,]$pilot_sourced_wholesale, 
    paired=TRUE); 
  
  pq_rsource[[s]] <- t.test(
    pq_sourcing_margin_analysis[rowIndices,]$finals_sourced_retail, 
    pq_sourcing_margin_analysis[rowIndices,]$pilot_sourced_retail, 
    paired=TRUE); 
  
  pq_wmarg[[s]] <- t.test(
    pq_sourcing_margin_analysis[rowIndices,]$finals_margin_wholesale, 
    pq_sourcing_margin_analysis[rowIndices,]$pilot_margin_wholesale, 
    paired=TRUE); 
  
  pq_rmarg[[s]] <- t.test(
    pq_sourcing_margin_analysis[rowIndices,]$finals_margin_retail, 
    pq_sourcing_margin_analysis[rowIndices,]$pilot_margin_retail,
    paired=TRUE); 
}

# Cleanup
rm(p, s, rowIndices, PARTICIPANTS, PURCHASE);

##########################################################################################
#
# Graphical analyses
#
##########################################################################################

PLATHEME<-(theme_bw() + 
             theme(text=element_text(size=18),strip.text=element_text(size=18)))

PLATHEME_BORDERLESS<-(PLATHEME + 
                        theme(panel.border = element_rect(color="white")))

OUTPATH<-"/Users/Markus/workspace/misq13/figures/"

abbnames<-function(str) {
  return( gsub("AstonTAC","As",
          gsub("CrocodileAgent","Cr",
          gsub("cwiBroker","Cw",
          gsub("default broker","*",
          gsub("Demokritac","Dm",
          gsub("INAOEBroker00","In",
          gsub("INAOEBroker02","In",
          gsub("LARGEpower","La",
          gsub("MLLBroker","Ml",
          gsub("TacTex","Ut",
          gsub("UTest","Ut",str))))))))))))
}

competitions<-plaf("
    SELECT  c.competition competition,
            c.comment tournament,
            participants,
            c.length length,
            signature
    FROM
            tmp.validcmp c,
            tmp.validcmp_signatures s
    WHERE
            c.competition = s.competition AND
            c.comment IN ('2013 Power TAC Finals', 
                          '2012 Power TAC Nuremberg Demo Games') AND
            NOT (c.comment = '2012 Power TAC Nuremberg Demo Games' AND signature LIKE 'C%')");

##########################################################################################
# 
# Analysis 1) Profitshare by Gamesize
#
##########################################################################################

# Load the final cash balances for all valid competitions
br_cash<-plaqcashfinal(competitions$competition)

br_cash$bkr_name<-factor(br_cash$bkr_name)
# UTest renamed to TacTex between 2012 and 2013
levels(br_cash$bkr_name)[levels(br_cash$bkr_name)=="INAOEBroker02"] <- "INAOE"
levels(br_cash$bkr_name)[levels(br_cash$bkr_name)=="default broker"] <- "Incumbent"
br_cash$bkr_name<-plyr::revalue(br_cash$bkr_name, c("UTest"="TacTex"));

# Summarize by computing the sum of all surplusses and the number of participants in each game 
br_cashsummary<-ddply(br_cash,~competition,summarise,cashsum=sum(balance[balance>0]))
br_cashsummary<-plyr::join(br_cash,br_cashsummary,type="inner")
br_cashsummary<-plyr::join(br_cashsummary,competitions,type="inner")
br_cashsummary$participants=factor(br_cashsummary$participants)
br_cashsummary[br_cashsummary$tournament == '2013 Power TAC Finals', 'tournament'] = '2013'
br_cashsummary[br_cashsummary$tournament == '2012 Power TAC Nuremberg Demo Games', 'tournament'] = '2012'

# Determine the marketshare as fraction of(positive) revenue over sum of all (positive) 
# revenues (surplus)
br_cashsummary$marketshare<-(pmax(br_cashsummary$balance,0) / pmax(br_cashsummary$cashsum,1))
# For the certainty we also include loss results
br_cashsummary$marketshare_nonabs<-(br_cashsummary$balance / pmax(br_cashsummary$cashsum,1))
# Include the rank in the competition according to marketshare
br_cashsummary<-ddply(br_cashsummary,.(competition), transform, rank=rank(1-marketshare))

# Determine the average profit share for each broker / game size combination
result<-ddply(br_cashsummary,.(bkr_name,tournament,participants),summarise,
              meanprofits=mean(marketshare),
              sdprofits=sd(marketshare_nonabs),
              observations=length(bkr_id))

# result$participants<-factor(result$participants, levels = rev(levels(result$participants)));
result$participants<-factor(result$participants, levels = levels(result$participants));

p<-ggplot(result, aes(y=participants, x=bkr_name, size=meanprofits / 2, color=sdprofits)) +
  geom_point() +
  scale_y_discrete("Game Size (# of Brokers)") +                     
  scale_x_discrete("Broker") +
  scale_size("Average Profit Share", range=c(1,35), guide="none") +
  scale_colour_gradient("", 
                        limits=c(0,1.1), breaks=c(0.1, 1.0), labels=c("0.1"="Certain", "1.0" = "Uncertain"), 
                        low="black", high="grey70", na.value="grey70") +
  facet_grid(tournament ~ .) +
  PLATHEME

ggsave(file=paste(OUTPATH,"misq_profitshare_by_gamesize.eps", sep=""), width=16.0, height=10.0)

rm(result, p)

##########################################################################################
# 
# Analysis 2) Retail Performance
#
##########################################################################################

br_retail<-plaf("
  SELECT  LEFT(comment, 4) comment, 
          bkr_name, purchase,
		      avg(volumeshare) avg_volumeshare, 
		      avg(normprice) avg_normprice, 
          avg(volumeuncertainty) avg_volumeuncertainty
  FROM
      (SELECT c.competition, comment, participants, length, tfc_purchase purchase,
              CASE bkr_name 
                WHEN 'UTest' THEN 'TacTex' 
                WHEN 'INAOEBroker02' THEN 'INAOE' 
                WHEN 'default broker' THEN 'Incumbent' 
                ELSE bkr_name END AS bkr_name, 
              tfc_sum_kwh / tfc_o_sum_kwh volumeshare,
              tfc_weighted_avg_price / tfc_o_weighted_avg_price normprice,
              tfc_stddev_kwh / tfc_o_stddev_kwh volumeuncertainty
      FROM
              tmp.validcmp c,
              tmp.tfc_stats t,
              tmp.tfc_overall_stats o,
              vpla_broker b
      WHERE 
              c.competition = t.competition AND
              c.competition = o.competition AND
              tfc_purchase = tfc_o_purchase AND
              broker = bkr_id) a
    WHERE
        comment IN ('2013 Power TAC Finals', 
                    '2012 Power TAC Nuremberg Demo Games')
    GROUP BY 
        comment, bkr_name, purchase");

br_retail$purchase[br_retail$purchase == 0]<-"Sale";
br_retail$purchase[br_retail$purchase == 1]<-"Purchase";
br_retail$purchase<-factor(br_retail$purchase, levels=c("Sale", "Purchase"));
br_retail$comment<-factor(br_retail$comment);

df.arrow = data.frame(xmin=c(0.5, 0.5), xmax=c(0.5, 0.5), 
                      ymin=c(7.0, 7.0), ymax=c(2.0, 2.0), 
                      purchase = factor(c("Sale", "Purchase"), levels=levels(br_retail$purchase)), 
                      comment = factor(c("2013", "2013"), levels=levels(br_retail$comment)),
                      bkr_name = c("", ""), 
                      avg_volumeuncertainty = c(0, 0));

df.text = data.frame(x=c(0.85, 0.85), y=c(7.0, 7.0), 
                      purchase = factor(c("Sale", "Purchase"), levels=levels(br_retail$purchase)), 
                      comment = factor(c("2013", "2013"), levels=levels(br_retail$comment)),
                      bkr_name = c("", ""), 
                      avg_volumeuncertainty = c(0.0, 0.0),
                      label=c("Compressed retail prices\nthrough fiercer competition",
                              "More brokers make use\nof small-scale production"));

p<-ggplot(br_retail, aes(x=avg_volumeshare, y=avg_normprice, legend=bkr_name, label=bkr_name, color=avg_volumeuncertainty)) +
  geom_text() +
  scale_x_continuous("Retail Market Share", limits=c(-0.1,1.1), breaks=c(0.0,0.5,1.0), labels=percent) +
  scale_y_sqrt("Price Level", limits=c(0.2,8.0), breaks=c(0.5,1.0,2.0,4.0), labels=c(".5x","1x","2x","4x")) +
  scale_colour_gradient("Certainty", guide="none", low="black", high="grey70", na.value="grey70") +
  geom_segment(data=df.arrow, aes(x=xmin, xend=xmax, y=ymin, yend=ymax), arrow=arrow(length=unit(0.4,"cm"))) +
  geom_text(data=df.text, aes(x=x, y=y, label=label), size=7) +
  facet_grid(comment ~ purchase) +
  PLATHEME

ggsave(file=paste(OUTPATH,"misq_retail_performance.eps", sep=""), width=16.0, height=10.0)

rm(p)

##########################################################################################
# 
# Analysis 3) Wholesale Performance
#
##########################################################################################

br_wholesale<-plaf("
  SELECT  LEFT(comment, 4) comment, 
          bkr_name, purchase,
  	      avg(volumeshare) avg_volumeshare, 
		      avg(normprice) avg_normprice, 
          avg(volumeuncertainty) avg_volumeuncertainty
  FROM
      (SELECT c.competition, comment, participants, length, mtr_purchase purchase,
              CASE bkr_name 
                WHEN 'UTest' THEN 'TacTex' 
                WHEN 'INAOEBroker02' THEN 'INAOE' 
                WHEN 'default broker' THEN 'Incumbent' 
                ELSE bkr_name END AS bkr_name, 
              mtr_sum_mwh / mtr_o_sum_mwh volumeshare,
              mtr_weighted_avg_price / mtr_o_weighted_avg_price normprice,
              mtr_stddev_mwh / mtr_o_stddev_mwh volumeuncertainty
      FROM
              tmp.validcmp c,
              tmp.mtr_stats m,
              tmp.mtr_overall_stats o,
              vpla_broker b
      WHERE 
              c.competition = m.competition AND
              c.competition = o.competition AND
              mtr_purchase = mtr_o_purchase AND
              broker = bkr_id) a
    WHERE
        comment IN ('2013 Power TAC Finals', 
                    '2012 Power TAC Nuremberg Demo Games')
    GROUP BY 
        comment, bkr_name, purchase");

br_wholesale$purchase[br_wholesale$purchase == 0]<-"Sale";
br_wholesale$purchase[br_wholesale$purchase == 1]<-"Purchase";
br_wholesale$purchase<-factor(br_wholesale$purchase, levels=c("Sale", "Purchase"));
br_wholesale$comment<-factor(br_wholesale$comment);

p<-ggplot(br_wholesale, aes(x=avg_volumeshare, y=avg_normprice, legend=bkr_name, label=bkr_name, color=avg_volumeuncertainty)) +
  geom_text(size=5) +
  scale_x_continuous("Wholesale Market Share", limits=c(-0.1,1.1), breaks=c(0.0,0.5,1.0), labels=percent) +
  scale_y_sqrt("Price Level", limits=c(0.5,1.5), breaks=c(0.5,1.0,1.5), labels=c(".5x","1x","1.5x")) +
  scale_colour_gradient("Certainty", guide="none", low="black", high="grey70", na.value="grey70") +
  facet_grid(comment ~ purchase) +
  PLATHEME

ggsave(file=paste(OUTPATH,"misq_wholesale_performance.eps", sep=""), width=16.0, height=10.0)

##########################################################################################
# 
# Analysis 4) Balancing Performance
#
##########################################################################################

br_balancing<-plaf("
  SELECT  LEFT(comment, 4) comment, 
          bkr_name, purchase,
      	  avg(volumeshare) avg_volumeshare,
    		  avg(normprice) avg_normprice,
    		  avg(volumeuncertainty) avg_volumeuncertainty
  FROM
      (SELECT c.competition, comment, participants, length, btr_purchase purchase,
        CASE bkr_name 
          WHEN 'UTest' THEN 'TacTex'  
          WHEN 'INAOEBroker02' THEN 'INAOE' 
          WHEN 'default broker' THEN 'Incumbent' 
          ELSE bkr_name END AS bkr_name, 
        btr_sum_kwh / btr_o_sum_kwh volumeshare,
			  btr_weighted_avg_price / btr_o_weighted_avg_price normprice,
			  btr_stddev_kwh / btr_o_stddev_kwh volumeuncertainty
      FROM
        tmp.validcmp c,
        tmp.btr_stats b,
        tmp.btr_overall_stats o,
        vpla_broker b
      WHERE 
        c.competition = b.competition AND
			  c.competition = o.competition AND
			  btr_purchase = btr_o_purchase AND
        b.broker = bkr_id) a
    WHERE
        comment IN ('2013 Power TAC Finals', 
                    '2012 Power TAC Nuremberg Demo Games')
    GROUP BY 
        comment, bkr_name, purchase");

br_balancing$purchase[br_balancing$purchase == 0]<-"Sale";
br_balancing$purchase[br_balancing$purchase == 1]<-"Purchase";
br_balancing$purchase<-factor(br_balancing$purchase, levels=c("Sale", "Purchase"));
br_balancing$comment<-factor(br_balancing$comment);

p<-ggplot(br_balancing, aes(x=avg_volumeshare, y=avg_normprice, legend=bkr_name, label=bkr_name, color=avg_volumeuncertainty)) +
  geom_text(size=5) +
  scale_x_continuous("Balancing Market Share", limits=c(-0.1,1.1), breaks=c(0.0,0.5,1.0), labels=percent) +
  scale_y_continuous("Price Level") +
  scale_colour_gradient("Certainty", guide="none", low="black", high="grey70", na.value="grey70") +
  facet_grid(comment ~ purchase, scales="free_y") +
  PLATHEME

ggsave(file=paste(OUTPATH,"misq_balancing_performance.eps", sep=""), width=16.0, height=10.0)

sval <- function(t, dig = 2){
  if(t$p.value < 0.05){
    sig <- "*";
  } else {
    sig <- " ";
  }

  return(paste("\\texttt{", as.character(round(t$estimate, digits = dig)), "\\textsuperscript{", sig, "}}", sep=""));  
}

pladone()

save.image("misq13.RData");