Power TAC Logfile Analysis - Anonymous Version
==============================================

Thank you for your interest in the Power TAC Logfile analysis framework. This page contains information on accessing the data and analyses used in publications that are currently under review.

You need several things to get started. We give instructions how to get each of them below.

* An account for the database that holds the data.
* The manual that describes the content of the database.
* The analysis scripts themselves.

Getting a database account
--------------------------

We use individualized accounts to control access levels and screen for misuse. To maintain your anonymity as a reviewer, please request an unnamed account through the editor (or through another proxy), mentioning only the paper you are reviewing. Formless requests can be sent to plaaccounts@econaissance.com.

Getting the manual
------------------

To better understand the contents of our analyses you might want to consult the PLA analysis manual that can be freely downloaded below.

Getting the scripts
-------------------

Our analyses can similarly be downloaded below. Make sure you download all files in the project. The easiest way to do this is with a git client (if you are a current git user). Otherwise, you can also use this web-based interface to download all top-level file and create the figures directory manually on your system.


In case of technical difficulties, do not hesitate to contact support@econaissance.com anonymously, for example through the editors, mentioning only the paper you are reviewing.

